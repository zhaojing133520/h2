"use strict"
function ClsPicOn(id, blurpic, word) { //新的阅读器
    //删掉其他内容
    document.getElementById("slide-out").style.display = "none"
    document.getElementById("main").style.display = "none"
    document.getElementById("footer").style.display = "none"
    if (isClsPicOpen) {//如果打开了图片的话
        setTimeout(function () {//文字框延时出现
            if (word != null && word != undefined && botm.isOpen) { //如果改用自己做的显示框框
                //关掉框框
                botm.options.onCloseEnd = () => {
                    botm.options.onCloseEnd = () => {
                        ClsPicOff();
                        document.getElementById("botm_content").innerHTML = '';
                    }
                }
                botm.close()
                //显示文字
                document.getElementById("test-active").innerHTML = '<div class="reader">' + word + '<div class="row center"><button class="waves-effect waves-light btn-flat" onclick="ClsPicOff()">关闭</button></div></div>';
                document.getElementById("test-active").style.display = "block";
                document.getElementById("test-active").scrollTo(0, 0);
                document.getElementById("test-active").style.animation = "0.5s cubic-bezier(.56,-0.18,.29,.94) 0s 1 normal forwards running jumpin";
                setTimeout(() => {//保留定位结果
                    document.getElementById("test-active").style.top = "0px"
                    document.getElementById("test-active").style.animation = ""
                }, 501);
            }
        }, 10);
    
    console.log("图片已打开，只进行改变")
    if (document.getElementById("test-active-bkgdiv").style.backgroundImage != "url(\"./img/" + id + ".jpg\")") { //并检测图片是否更改过。如更改过，渐变更改为另一个
        document.getElementById("test-active-bkgdiv").style.animation = "fadeaway 0.3s forwards"
        setTimeout(() => {//填入新图片并显示
            document.getElementById("test-active-bkgdiv").style.backgroundImage == "url(\"./img/" + id + ".jpg\")";
            document.getElementById("test-active-bkgdiv").style.animation = "fadein 0.3s forwards"
        }, 301);
    }
    if (word != document.getElementById("test-active").innerHTML) {//再检测文字改变过没有，改变过的话渐变
        document.getElementById("test-active").style.animation += "fadeaway 0.3s forwards"
        setTimeout(() => {//填入新文字并显示
            document.getElementById("test-active").innerHTML = '<div class="reader">' + word + '<div class="row center"><button class="waves-effect waves-light btn-flat" onclick="ClsPicOff()">关闭</button></div></div>';
            document.getElementById("test-active").style.animation = "fadein 0.3s forwards"
            setTimeout(() => {
                document.getElementById("test-active").style.animation = "";
            }, 300);
            document.getElementById("test-active").scrollTo(0, 0);
        }, 301);
    }
} else {
    //关掉小把手
    document.getElementById("slide-menu").style.display = "none"
    //提交图片打开了的提示
    isClsPicOpen = true;
    //图片从底部弹出
    document.getElementById("test-active-bkgdiv").style.left = "0px";
    document.getElementById("test-active-bkgdiv").style.top = document.defaultView.getComputedStyle(document.body, null)["height"];
    //文字从底部弹出
    document.getElementById("test-active").style.left = "0px";
    document.getElementById("test-active").style.top = document.defaultView.getComputedStyle(document.body, null)["height"];
    //设置图片
    document.getElementById("test-active-bkgdiv").style.backgroundImage = "url(\"./img/" + id + ".jpg\")"
    document.getElementById("test-active-bkgdiv").style.display = "block";
    //图片的动画
    document.getElementById("test-active-bkgdiv").style.animation += "0.5s cubic-bezier(.56,-0.18,.29,.94) 0s 1 normal forwards running jumpin";
    setTimeout(() => {
        document.getElementById("test-active-bkgdiv").style.top = "0px";//保留第一个动画的成果
        if (blurpic == "false" || blurpic == false || blurpic === undefined || blurpic === null) { //判断是否模糊
            document.getElementById("test-active-bkgdiv").style.animation = "blurin 0.1s forwards";
            setTimeout(() => {//保留模糊结果
                document.getElementById("test-active-bkgdiv").style.filter = "blur(10px)";
                document.getElementById("test-active-bkgdiv").style.animation = "";
            }, 101);
        } else {
            document.getElementById("test-active-bkgdiv").style.animation = ""
        }
    }, 501);
    setTimeout(function () {//文字框延时出现
        if (word != null && word != undefined) { //如果使用自己做的显示框框
            //显示文字
            document.getElementById("test-active").innerHTML = '<div class="reader">' + word + '<div class="row center"><button class="waves-effect waves-light btn-flat" onclick="ClsPicOff()">关闭</button></div></div>';
            document.getElementById("test-active").style.display = "block";
            document.getElementById("test-active").scrollTo(0, 0);
            document.getElementById("test-active").style.animation = "0.5s cubic-bezier(.56,-0.18,.29,.94) 0s 1 normal forwards running jumpin";
            setTimeout(() => {//保留定位结果
                document.getElementById("test-active").style.top = "0px"
                document.getElementById("test-active").style.animation = ""
            }, 501);
        }
    }, 10);
}
}

function ClsPicOff() {//关掉拟态框，这个是两个系统中不通用的
    if (isClsPicOpen) {
        isClsPicOpen = false;
        document.getElementById("test-active").style.animation = "0.5s cubic-bezier(.56,-0.18,.29,.94) 0s 1 normal forwards running jumpout";
        document.getElementById("test-active-bkgdiv").style.animation = "0.5s cubic-bezier(.56,-0.18,.29,.94) 0s 1 normal forwards running jumpout";
        setTimeout(function () {
            document.getElementById("test-active").style.display = "none";
            document.getElementById("test-active-bkgdiv").style.display = "none";
            document.getElementById("slide-out").style.display = "block"
            document.getElementById("slide-out").style.animation = "fadein 0.3s forwards";
            document.getElementById("main").style.display = "block"
            document.getElementById("main").style.animation = "fadein 0.3s forwards";
            document.getElementById("footer").style.display = "block"
            document.getElementById("footer").style.animation = "fadein 0.3s forwards";
            setTimeout(function () {
                document.getElementById("test-active").style.animation = "";
                document.getElementById("test-active-bkgdiv").style.animation = "";
                document.getElementById("slide-out").style.animation = "";
                document.getElementById("main").style.animation = "";
                document.getElementById("footer").style.animation = "";
                document.getElementById("test-active-bkgdiv").style.filter = "";
                document.getElementById("test-active").innerHTML = "";
            }, 100);
        }, 300);
    }
}

function reader(node, jscode) {//新的阅览器,支持Markdown；
    localStorage.setItem("lastread", node)
    var xhr = new XMLHttpRequest();
    xhr.open("GET", './reader/' + node + '.json', true);
    xhr.send();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                let ans = JSON.parse(xhr.responseText);
                if (ans.isMarkDown == "true" || ans.isMarkDown == true) {
                    ClsPicOn(ans.id, localStorage.getItem("setting-pic"), marked(ans.content));
                } else {
                    ClsPicOn(ans.id, localStorage.getItem("setting-pic"), ans.content)
                }

                location.hash = "#c"
                location.hash = "#reader/" + node;
                if (jscode !== null && jscode !== undefined) { //如果有异步操作
                    jscode.apply();
                }
            } else {
                alert("网络错误，代码" + xhr.status);
            }
        }
    }
}