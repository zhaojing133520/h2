export function clsup_e() {
    clsshow(
        "新室友",
        "<p>当你今天下班回家时，你的女朋友给了你一个惊喜-你们有了一个新的室友!是那个在万圣节派对遇到的白头发女孩安德莉亚。<br>看来这两个人认识，安德里亚需要一个地方住一段时间，你们正好有一个大公寓。<br>她去了另一个房间换式服，你的女朋友向你们俩打了个招呼，然后就去跑步了。</p><p>-让新室友整理行李（有可能得到你的妹子允许你和安德里亚搞在一起，heart-1这个效果仅在heart>4时才会进行）<br>-看看她（slut+1）</p>",
        0,
        "U4/U4_4",
        [{ "btnfun": "clsfin('U4/U4_4','luddage')", "btnname": "让新室友整理行李" }, { "btnfun": "clsfin('U4/U4_4','see')", "btnname": "看看她" }]
    );
}
export function clsfin_e(info) {
    switch (info) {
        case "luddage": {
            if (parseInt(localStorage.getItem("heart")) < 4) {
                clsshow(
                    "新室友",
                    "<p>你的Heart&lt;4，不能请求妹子的同意。</p>",
                    0,
                    "U4/U4_4",
                    [{ "btnfun": "clsfin('U4/U4_4','e')", "btnname": "继续" }]
                );
            } else {
                heart(-1)
                if (localStorage.getItem("couple") == "1") {
                    if (Math.random() < 0.5) {
                        clsshow(
                            "新室友",
                            "<p>和你的妹子谈论安吉利亚，嗯，她同意你和她搞在一起啦！</p>",
                            0,
                            "U4/U4_4",
                            [{ "btnfun": "clsfin('U4/U4_4','e')", "btnname": "继续" }]
                        );
                        localStorage.setItem("house-adly", true);
                    } else {
                        clsshow(
                            "新室友",
                            "<p>和你的妹子谈论安吉利亚，她不同意你和她搞在一起。1小时后再来吧</p>",
                            0,
                            "U4/U4_4",
                            [{ "btnfun": "clsfin('U4/U4_4','e')", "btnname": "继续" }]
                        );
                    }
                } else {
                    if (Math.random() < 0.166666666666) {
                        clsshow(
                            "新室友",
                            "<p>和你的妹子谈论安吉利亚，嗯，她同意你和她搞在一起啦！</p>",
                            0,
                            "U4/U4_4",
                            [{ "btnfun": "clsfin('U4/U4_4','e')", "btnname": "继续" }]
                        );
                        localStorage.setItem("house-adly", true);
                    } else {
                        clsshow(
                            "新室友",
                            "<p>和你的妹子谈论安吉利亚，她不同意你和她搞在一起。1小时后再来吧</p>",
                            0,
                            "U4/U4_4",
                            [{ "btnfun": "clsfin('U4/U4_4','e')", "btnname": "继续" }]
                        );
                    }
                }
            }
            localStorage.setItem("cls", "U4_7");
            break;
        }
        case "see": {
            localStorage.setItem("cls", "U4_6")
            /*dialogAlert("本关卡完成", "成功");*/
            ClsPicOff();
            app.initia();
            break;
        }
        case "e": {
            /*dialogAlert("本关卡完成", "成功");*/
            ClsPicOff();
            app.initia();
        }
    }

}


/*
 <a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_0_0\',\'0\')">继续</a>
*/