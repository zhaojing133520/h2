export  function clsup_e(){
    clsshow(
        "派对狂人",
        "<p>在大厅的角落，卧室门前。站着一个很明显喝醉的男人。</p><p>“哦吼，玩的开心吗？看，这门后才是真正的派对。而我可以决定谁才能进去。想进去可要付出点代价哦。</p><br><p>-你只想路过（slut+1）<br></p>-和他喝一杯（alpha+1）<br>-进入卧室</p>",
        0,
        "U2/U2_24",
        //' <a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_24\',\'pass\')">你只想路过</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_24\',\'drink\')">和他喝一杯</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_24\',\'room\')">进入卧室</a>'
        [{"btnfun":"clsfin(\'U2/U2_24\',\'pass\')","btnname":"你只想路过"},{"btnfun":"clsfin(\'U2/U2_24\',\'drink\')","btnname":"和他喝一杯"},{"btnfun":"clsfin(\'U2/U2_24\',\'room\')","btnname":"进入卧室"}]
    )
 }

 export function clsfin_e(info){
     switch(info){
         case "pass":{//路过
            slut(1)
             clsshow(
                 "派对狂人",
                 "<p>亲吻洗碗海绵2分钟</p>",
                 120,
                 "U2/U2_24",
                 //'<a href="#!" class="modal-close waves-effect waves-green btn-flat disabled timeset" onclick="clsfin(\'U2/U2_24\',\'done\')">完成</a>'
                 [{"btnfun":"clsfin(\'U2/U2_24\',\'done\')","btnname":"完成"}]
             )
             break;
         }
         case "drink":{//喝一杯
            alpha(1);
            clsshow(
                "派对狂人",
                "<p>以最快速度喝500ml啤酒</p>",
                5,
                "U2/U2_24",
                //'<a href="#!" class="modal-close waves-effect waves-green btn-flat disabled timeset" onclick="clsfin(\'U2/U2_24\',\'done\')">完成</a>'
                [{"btnfun":"clsfin(\'U2/U2_24\',\'done\')","btnname":"完成"}]
            )
            break;
        }
        case "room":{//卧室
            clsshow(
                "派对狂人",
                "<p>你将失去你的内裤（这是全自动的）</p>",
                0,
                "U2/U2_24",
                //'<a href="#!" class="modal-close waves-effect waves-green btn-flat disabled timeset" onclick="clsfin(\'U2/U2_24\',\'done2\')">完成</a>'
                [{"btnfun":"clsfin(\'U2/U2_24\',\'done2\')","btnname":"完成"}]
            )
            //如果有内裤则移除
            if(localStorage.getItem("item").indexOf("P0")>-1){
                itemremove2("P0")
            }
            if(localStorage.getItem("item").indexOf("P1")>-1){
                itemremove2("P1")
            }
            if(localStorage.getItem("item").indexOf("P2")>-1){
                itemremove2("P2")
            }
            break;
        }
        case "done":{
            localStorage.setItem("cls", "U2_25");
            /*dialogAlert("本关卡完成", "成功");*/
            ClsPicOff();
            app.initia();
            break;
        }
        case "done2":{
            localStorage.setItem("cls", "U2_26");
            /*dialogAlert("本关卡完成", "成功");*/
            ClsPicOff();
            app.initia();
            break;
        }
        default: {
            dialogAlert("系统错误，请点击课程卡片获取课程编码并向群里报告此问题")
            break;
        }
     }
 }