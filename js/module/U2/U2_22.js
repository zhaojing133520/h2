export function clsup_e() {
    clsshow(
        "猜谜游戏",
        "<p>在走廊中间，你看一群女孩在墙边排队，所有人都蒙着眼睛，插入形形色色的性具。旁边的人告诉你这很简单：蒙上眼睛，随便选一个性具，插入或者吮吸它。1分钟猜出它的名称、长度或直径。</p><br><p>猜出一个名称即可通过挑战，第一次猜出超过3个道具的名称则可获得$10如果穿着了精灵套装允许长度有1CM直径有0.5CM误差</p>",
        0,
        "U2/U2_22",
        //'<a href="#!" class="waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_22\',\'10\')">增加10块钱</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_22\',\'done\')">完成</a>'
        [{"btnfun":"clsfin(\'U2/U2_22\',\'10\')","btnname":"增加10块钱"},{"btnfun":"clsfin(\'U2/U2_22\',\'done\')","btnname":"完成"}]
    );
}
export function clsfin_e(info) {
    switch (info){
        case "10":{
            money(10);
            dialogAlert("成功增加10块钱","成功")
            break;
        }
        case "done": {
            if(localStorage.getItem("buff-U2")){
                if(localStorage.getItem("buff-U2").indexOf("U2_22") == -1){
                        localStorage.setItem("buff-U2",localStorage.getItem("buff-U2")+"/meow/U2_22");
                }
            }else{
                localStorage.setItem("buff-U2", "U2_22");
            }
            clsshow(
                "猜谜游戏",
                "<p>请选择下一步进入哪个关卡</p><p>部分关卡具有可重复性，不过，你的进度将回到该关卡。</p><p>你可以在帮助信息中找到关卡解释图</p>",
                0,
                "U2/U2_22",
                //' <a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_22\',\'party\')">前往“派对狂人”关卡</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_22\',\'fee\')">前往“入场费”关卡</a>'
                [{"btnfun":"clsfin(\'U2/U2_22\',\'party\')","btnname":"前往“派对狂人”关卡"},{"btnfun":"clsfin(\'U2/U2_22\',\'fee\')","btnname":"前往“入场费”关卡"}]
            );
            break;
        }
        case "party":{
            localStorage.setItem("cls", "U2_24");
            /*dialogAlert("本关卡完成", "成功");*/
            ClsPicOff();
            app.initia();
            break;
        }
        case "fee":{
            localStorage.setItem("cls", "U2_20");
            /*dialogAlert("本关卡完成", "成功");*/
            ClsPicOff();
            app.initia();
            break;
        }
        default: {
            dialogAlert("系统错误，请点击课程卡片获取课程编码并向群里报告此问题");
            break;
        }
    }
}


/* 
 <a href="#!" class="waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_16\',\'1\')">增加1块钱</a>
   <a href="#!" class="waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_16\',\'5\')">增加5块钱</a>
  <a href="#!" class="waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_16\',\'10\')">增加10块钱</a>
<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_16\',\'done\')">完成</a>
*/