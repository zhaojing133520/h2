export function clsup_e() {
    clsshow(
        "派对时间",
        "<p>屋子周围全都是人，乱糟糟的挤在门厅，试图进入屋内。</p><p>你刚进入室内，你的妹子就冲进人群中，你一下子就找不到她了。好吧，看来你只能靠自己了。</p><p>部分关卡具有可重复性，不过，你的进度将回到该关卡。</p><p>你可以在帮助信息中找到关卡解释图</p>",
        0,
        "U2/U2_10",
        //' <a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_10\',\'0\')">前往“喝尿的母狗”关卡</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_10\',\'1\')">前往“在大厅中揉弄”关卡</a>'
        [{"btnfun":"clsfin(\'U2/U2_10\',\'0\')","btnname":"前往“喝尿的母狗”关卡"},{"btnfun":"clsfin(\'U2/U2_10\',\'1\')","btnname":"前往“在大厅中揉弄”关卡"}]
    );
}
export function clsfin_e(info) {
    localStorage.setItem("buff-party", "yes");
    switch (info) {
        case "0": {
            localStorage.setItem("cls", "U2_11");
            break;
        }
        case "1": {
            localStorage.setItem("cls", "U2_12");
            break;
        }
        default: {
            dialogAlert("系统错误，请点击课程卡片获取课程编码并向群里报告此问题");
            break;
        }
    }
    /*dialogAlert("本关卡完成", "成功");*/
    ClsPicOff();
    app.initia();
}


/*
 <a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_0_0\',\'0\')">继续</a>
*/