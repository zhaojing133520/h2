export function clsup_e() {
    clsshow(
        "洗劫冰箱",
        "<p>在厨房的这边，靠近水池，另一场比赛正在举行。</p><p>一个女孩双手被绑在厨房里，后面插着一根管子，眼睛泪汪汪的。看来她身体里已经有不少水了，不久，便喷涌而出。人群爆发出一阵欢呼。</p><p>目前冠军可以灌肠1L，如果你打败了她，每多250ml可得到$5</p><p>不要太强迫自己哦。</p><p>使用灌肠设备，打破冠军记录得到$10,每多250ml获得$5,当你到达自己的极限时每坚持1分钟获得$1</p>",
        0,
        "U2/U2_17",
        //'  <a href="#!" class="waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_17\',\'1\')">增加1块钱</a><a href="#!" class="waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_17\',\'5\')">增加5块钱</a><a href="#!" class="waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_17\',\'10\')">增加10块钱</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_17\',\'done\')">完成</a>'
        [{"btnfun":"clsfin(\'U2/U2_17\',\'1\')","btnname":"增加1块钱"},{"btnfun":"clsfin(\'U2/U2_17\',\'5\')","btnname":"增加5块钱"},{"btnfun":"clsfin(\'U2/U2_17\',\'10\')","btnname":"增加10块钱"},{"btnfun":"clsfin(\'U2/U2_17\',\'done\')","btnname":"完成"}]
    );
}
export function clsfin_e(info) {
    switch (info){
        case "1":{
            money(1);
            dialogAlert("成功增加1块钱","成功")
            break;
        }
        case "5":{
            money(5);
            dialogAlert("成功增加5块钱","成功")
            break;
        }
        case "10":{
            money(10);
            dialogAlert("成功增加10块钱","成功")
            break;
        }
        case "done": {
            if(localStorage.getItem("buff-U2")){
                if(localStorage.getItem("buff-U2").indexOf("U2_17") == -1){
                        localStorage.setItem("buff-U2",localStorage.getItem("buff-U2")+"/meow/U2_17");
                }
            }else{
                localStorage.setItem("buff-U2", "U2_17");
            }
            clsshow(
                "洗劫冰箱",
                "<p>请选择下一步进入哪个关卡</p><p>部分关卡具有可重复性，不过，你的进度将回到该关卡。</p><p>你可以在帮助信息中找到关卡解释图</p>",
                0,
                "U2/U2_17",
                //' <a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_17\',\'eat\')">前往“餐厅”关卡</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_17\',\'fridge\')">前往“洗劫冰箱”关卡</a>'
                [{"btnfun":"clsfin(\'U2/U2_17\',\'eat\')","btnname":"前往“餐厅”关卡"},{"btnfun":"clsfin(\'U2/U2_17\',\'fridge\')","btnname":"前往“洗劫冰箱”关卡"}]
            );
            break;
        }
        case "eat":{
            localStorage.setItem("cls", "U2_15");
            /*dialogAlert("本关卡完成", "成功");*/
            ClsPicOff();
            app.initia();
            break;
        }
        case "fridge":{
            localStorage.setItem("cls", "U2_16");
            /*dialogAlert("本关卡完成", "成功");*/
            ClsPicOff();
            app.initia();
            break;
        }
        default: {
            dialogAlert("系统错误，请点击课程卡片获取课程编码并向群里报告此问题");
            break;
        }
    }
}


/* 
 <a href="#!" class="waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_16\',\'1\')">增加1块钱</a>
   <a href="#!" class="waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_16\',\'5\')">增加5块钱</a>
  <a href="#!" class="waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_16\',\'10\')">增加10块钱</a>
<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_16\',\'done\')">完成</a>
*/