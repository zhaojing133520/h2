export function clsup_e() {
    clsshow(
        "总结",
        "<p>当你进入公寓时，她在你关上门之前就开始脱掉身上被汗浸湿的衣服。</p><p>“那么，我们应该谈谈昨晚的事。派对怎么样？很开心吧？”</p><p>她没有等待回答，继续脱着衣服。</p><br><p>-你觉得很有趣，她也许会开心(slut+2)</p><p>-你觉得过的很糟糕，她也许会伤心(alpha+1)</p><p>-你并不喜欢性爱派对，她或许或生气(sissy+2,slut-4)</p>",
        0,
        "U3/U3_7",
        //'<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U3/U3_7\',\'interesting\')">有趣</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U3/U3_7\',\'bad\')">糟糕</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U3/U3_7\',\'dislike\')">不喜欢</a>'
        [{ "btnfun": "clsfin(\'U3/U3_7\',\'interesting\')", "btnname": "有趣" }, { "btnfun": "clsfin(\'U3/U3_7\',\'bad\')", "btnname": "糟糕" }, 
        { "btnfun": "clsfin(\'U3/U3_7\',\'dislike\')", "btnname": "不喜欢" }]
    );
}
export function clsfin_e(info) {
    switch (info) {
        case "interesting": {
            slut(2);
            break;
        }
        case "bad": {
            alpha(1);
            break;
        }
        case "dislike": {
            sissy(2);
            slut(-4);
            break;
        }
        default: {
            dialogAlert("系统错误，请点击课程卡片获取课程编码并向群里报告此问题");
            break;
        }
    }
    if (localStorage.getItem("buff-U3-6with")) {
        localStorage.setItem("cls", "U3_8");
        localStorage.removeItem("buff-U3-6with")
    } else {
        localStorage.setItem("cls", "U3_9");
    }
    /*dialogAlert("本关卡完成", "成功");*/
    ClsPicOff();
    app.initia();
}
